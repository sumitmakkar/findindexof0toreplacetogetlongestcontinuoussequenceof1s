#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arrVector;
        int         prevIndex;
        int         prevPrevIndex;
        int         maxLen;
        int         requiredIndex;
    
    public:
        Engine(vector<int> aV)
        {
            arrVector     = aV;
            prevIndex     = -1;
            prevPrevIndex = -1;
            maxLen        = -1;
            requiredIndex = -1;
        }
    
        int findIndexOfZeroForLongestSequenceOfOnes()
        {
            int len     = (int)arrVector.size();
            int currLen = -1;
            for(int currIndex = 0 ; currIndex <len ; currIndex++)
            {
                if(!arrVector[currIndex])
                {
                    if(prevPrevIndex != -1)
                    {
                        currLen = currIndex - prevPrevIndex - 1;
                        if(currLen > maxLen)
                        {
                            maxLen = currLen;
                            requiredIndex = prevIndex;
                        }
                    }
                    prevPrevIndex = prevIndex;
                    prevIndex     = currIndex;
                }
            }
            return requiredIndex;
        }
};

int main(int argc, const char * argv[])
{
    vector<int> arrVector = {0,0,1,1,1,1,0,0,1,1,1,1,0,1,1,0,1,0,1,1,1,1,0};
    Engine      e         = Engine(arrVector);
    cout<<e.findIndexOfZeroForLongestSequenceOfOnes()<<endl;
    return 0;
}
